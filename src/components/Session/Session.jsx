import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import styles from './Session.module.scss';

const Session = ({ session }) => {
  const [instructor, setInstructor] = useState();
  const [place, setPlace] = useState();

  const getInstructor = async () => {
    const response = await fetch(`/api/users/${session.instructorUserId}`);
    if (response.ok) {
      const data = await response.json();
      setInstructor(data);
    }
  };

  const getPlace = async () => {
    const response = await fetch(`/api/places/${session.placeId}`);
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setPlace(data);
    }
  };

  useEffect(() => {
    console.log(session);
    getInstructor();
    getPlace();
  }, []);

  return (
    <div className={styles.session}>
      <div className={styles.textContainer}>
        <h3 className={styles.date}>Le {parseInt(new Date(session.dateStart).getDate()) < 10 ? ('0'+(new Date(session.dateStart).getDate())) : (new Date(session.dateStart).getDate())}/{parseInt(new Date(session.dateStart).getMonth()) < 10 ? ('0'+(new Date(session.dateStart).getMonth())) : (new Date(session.dateStart).getMonth())}/{new Date(session.dateStart).getFullYear()} de {new Date(session.dateStart).getHours()}h à {new Date(session.dateEnd).getHours()}h</h3>
        <p className={styles.instructor}>avec {instructor?.firstName}</p>
        <p className={styles.adresse}>{place?.name}</p>
      </div>
      <div className={styles.buttonContainer}>
        <Link className={styles.button} to={`/cours/${session.id}`}>Réserver ce cours</Link>
        <Link className={styles.button} to={`/monitor/${instructor?.id}`}>Voir le moniteur</Link>
      </div>
    </div>
  );
};

export default Session;
