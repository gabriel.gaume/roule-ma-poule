import { Link } from 'react-router-dom';
import logo from '../../assets/icons/logo.svg';
import styles from './Footer.module.scss';

const Footer = () => (
  <footer className={styles.footer}>
    <div className={styles.header}>
      <img className={styles.logo} src={logo} alt="" />
      <h2 className={styles.hero}>Roule ma poule</h2>
    </div>
    <ul className={styles.list}>
      <li>
        <Link className={styles.link} to="/">Se connecter</Link>
      </li>
      <li>
        <Link className={styles.link} to="/">S'inscrire</Link>
      </li>
      <li>
        <Link className={styles.link} to="/">CGV</Link>
      </li>
      <li>
        <Link className={styles.link} to="/">Mentions Légales</Link>
      </li>
      <li>
        <Link className={styles.link} to="/">Politique de confidentialité</Link>
      </li>
    </ul>
  </footer>
);

export default Footer;
