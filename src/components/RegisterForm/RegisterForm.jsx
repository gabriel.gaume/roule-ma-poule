import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styles from './RegisterForm.module.scss';

const LoginForm = props => {

    const {
        onSubmit,
        changeHasAccount
    } = props


    const [formState, setFormState] = useState({
        email: '',
        role: '',
        password: '',
        confirmPassword: '',
    })

    const handleFormChange = e => {
        setFormState({
            ...formState,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = evt => {
        evt.preventDefault();
        if (formState.password !== '' && formState.password === formState.confirmPassword) {
          return onSubmit(formState)
        }
        alert('Les mots de passe ne correspondent pas');
    }

    return <form className={styles.form} onSubmit={handleSubmit}>
    <label className={styles.label} htmlFor="email">Adresse e-mail</label>
    <input
        className={styles.input} 
        name="email" 
        type="email" 
        placeholder="Entrez votre e-mail" 
        value={formState.email} 
        onChange={handleFormChange}
    />
    <label className={styles.label} htmlFor="selector">Vous êtes :</label>
    <select className={styles.input} name="selector" onChange={handleFormChange}>
        <option>Votre rôle</option>
        <option value="instructor">Moniteur</option>
        <option value="student">Elève</option>
    </select>
    <label className={styles.label} htmlFor="password">Entrer votre mot de passe</label>
    <input
        className={styles.input} 
        name="password" 
        type="password" 
        placeholder="Mot de passe" 
        value={formState.password}
        onChange={handleFormChange}
    />
    <label className={styles.label} htmlFor="password">Confirmer votre mot de passe</label>
    <input
        className={styles.input}
        name="confirmPassword" 
        type="password" 
        placeholder="Mot de passe" 
        value={formState.confirmPassword}
        onChange={handleFormChange}
    />
    <button className={styles.button}>S'inscrire</button>
    <p className={styles.subbutton} onClick={changeHasAccount}>Vous avez déjà un compte ?</p>
    </form>
}

LoginForm.propTypes = {
    disabled: PropTypes.bool,
    onSubmit: PropTypes.func
}

export default LoginForm
