import React, {useState, useEffect} from 'react'
import Map from '../../Map';
import styles from './DashboardStudent.module.scss';

const DashboardStudent = (props) => {

    const {
        idUser
    } = props

    const [student, setStudent] = useState([])
    useEffect(() => {
        
        fetch('/api/users/' + idUser)
            .then(res => res.json())
            .then(data => setStudent(data))
    }, [idUser]);

    
    const [sessions, setSessions] = useState([])
    const [instructors, setInstructors] = useState([])
    const [places, setPlaces] = useState([])

    useEffect(() => {
        
        fetch('/api/sessions?studentUserId=' + idUser)
            .then(res => res.json())
            .then(data => setSessions(data))

        fetch('/api/users/')
            .then(res => res.json())
            .then(data => setInstructors(data))

        fetch('/api/places/')
            .then(res => res.json())
            .then(data => setPlaces(data))
    }, [idUser]);

    let nextSession = sessions[0]
    
    let nextInstructor = ''
    let nextPlace = ''

    if (nextSession) {
      instructors.filter(instructor => instructor.id === nextSession.instructorUserId ).map(instructor => 
          nextInstructor = instructor
      )
      places.filter(place => place.id === nextSession.placeId ).map(place => 
          nextPlace = place
      )
    }

    // config pour le format de la date
    const options = {year: 'numeric', month: 'numeric', day: 'numeric' };

    let progress = ''
    if(student.id) {
        progress = student.competences.length
    }

    return  <div className={styles.dashStudent}>
                <h3 className={styles.dashStudentTitle}>Bonjour, {student.firstName} {student.lastName}</h3>
                <div className={styles.dashStudentProgress}>
                    <p className={styles.dashStudentProgressTitle}>Ma progression</p>
                    {
                        progress !== '' ? <div><progress className={styles.studentProgress} id="file" max="90" value={progress * 10}></progress> <p>{progress * 10}%</p></div> : ''
                    }
                </div>
                
                <section className={styles.section}>
                    {nextSession && (<>
                      <div className={styles.lesson}>
                        <p className={styles.lessonTitle}>Prochaine leçon :</p>

                        <p>Votre prochain cours est </p>
                        {
                          nextSession && <p>le {new Date(nextSession.dateStart).toLocaleTimeString("fr-FR", options).slice(0, -10)} de {new Date(nextSession.dateStart).toLocaleTimeString().slice(0, 2)}h à {new Date(nextSession.dateEnd).toLocaleTimeString().slice(0, 2)}h </p>
                        }
                        
                        <p>avec {nextInstructor.firstName} {nextInstructor.lastName}</p>
                    
                        <div className={styles.lesson2}>
                            <p>Rendez-vous :</p>
                            <p>{nextPlace.name}, 17000 La Rochelle</p>
                        </div>
                      </div>
                    
                      <div className={styles.map}>
                      {
                        nextPlace &&
                          <Map idPlaces={[nextPlace.id]} heightPlace={'26vh'} widthPlace={'100%'}/>
                      }
                      </div>
                    </>)}
                </section>
            </div>
}

export default DashboardStudent