import React, { useState, useEffect } from 'react'
import styles from './Infos.module.scss'
const Infos = (props) => {

    const {
        idUser,
        userType
    } = props

    const [user, setUser] = useState([])
    useEffect(() => {
        fetch('/api/users/' + idUser)
            .then(res => res.json())
            .then(data => setUser(data))
    }, [idUser]);


    const [currentTab, setCurrentTab] = useState(1)

    return <div>
        <form className={styles.form} action="">
            <section className={`${styles.form__section} ${styles.form__identite}`}>
                <h3 className={styles.form__soustitre}>Identité</h3>
                <div>
                    <label htmlFor="">Prénom</label>
                    <input type="text" placeholder={user.firstName} />
                </div>
                <div>
                    <label htmlFor="">Nom</label>
                    <input type="text" placeholder={user.lastName} />
                </div>
                <div>
                    <label htmlFor="">Email</label>
                    <input type="text" placeholder={user.email} />
                </div>
                <div>
                    <label htmlFor="">Téléphone</label>
                    <input type="text" placeholder='0656345621' />
                </div>
                <div>
                    <label htmlFor="">Date de naissance</label>
                    <input className={styles.form__inputAuto} type="date" />
                </div>
            </section>

            <section className={`${styles.form__section} ${styles.form__sectionImg}`}>
                <img src={`${process.env.PUBLIC_URL}/moniteurs/${user.image}`} alt="" className={styles.form__img} />
                <a className={styles.form__btn}>Ajouter une photo de profil</a>
            </section>

            <section className={`${styles.form__section} ${styles.form__documents}`}>
                <h3 className={styles.form__soustitre}>Documents</h3>
                <div className={styles.form__documentSection}>
                    <p>Pièce d'identité</p>
                    <a href="">Ajouter</a>
                </div>
                <div className={styles.form__documentSection}>
                    <p>Preuve d’obtention du code de la route</p>
                    <a href="">Ajouter</a>
                </div>
            </section>

            <section className={`${styles.form__section} ${styles.form__paiement}`}>
                <h3 className={styles.form__soustitre}>Paiement</h3>
                <div>
                    <label htmlFor="">N° de carte</label>
                    <input className={styles.form__inputAuto} type="text" placeholder="47** **** **** **83" />
                </div>
                <div>
                    <label htmlFor="">Date d'expiration</label>
                    <input className={styles.form__inputAuto} type="date" />
                </div>
                <div>
                    <label htmlFor="">Cryptogramme Visuel</label>
                    <input className={styles.form__inputAutoCrypto} type="text" placeholder="***" />
                </div>
                <div>
                    <img src="" alt="" />
                    <img src="" alt="" />
                    <img src="" alt="" />
                </div>
            </section>
            <div className={styles.enregistrer}>
                <button className={styles.form__btn} type='submit'>Enregister</button>
            </div>
        </form>

    </div>
}

export default Infos