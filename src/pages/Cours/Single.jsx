import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Popup from 'react-customizable-popup';
import Map from "../../components/Map";
import arrow from '../../assets/icons/arrowOrange.svg';
import styles from './SingleCours.module.scss';

const Single = () => {
    const { id } = useParams()
    const [reservation, setReservation] = useState(null)
    const [isResponsive, setIsResponsive] = useState();
    const [isFirst, setFirst] = useState(false)

    useEffect(() => {
        fetch('/api/sessions/' + id)
            .then(res => res.json())
            .then(data => setReservation(data))
            if (window.innerWidth < 1024) {
                setIsResponsive(true)
            } else {
                setIsResponsive(false)
            }  
    }, [id])

    const [users, setUsers] = useState([])
    useEffect(() => {
        fetch('/api/users')
            .then(res => res.json())
            .then(data => setUsers(data))
    }, []);

    const [places, setPlaces] = useState([])
    useEffect(() => {
        fetch('/api/places')
            .then(res => res.json())
            .then(data => setPlaces(data))
    }, []);

    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    const opt_weekday = { weekday: 'long' };
    function toTitleCase(str) {
        return str.replace(
            /\w\S*/g,
            function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );
    }

    function handleclick() {
        if(isFirst === false){
            setFirst(true)
        } else {
            setFirst(false)
        }
        
    }

    return (
        <>{reservation &&
            <div className={styles.content}>
                <div className={styles.desc}>
                <Link  to={'/student'}><img className={styles.arrow} src={arrow} alt="" /></Link>
                <h2 className={styles.title}>Le {toTitleCase(new Date(reservation.dateEnd).toLocaleDateString("fr-FR", opt_weekday))} {new Date(reservation.dateStart).toLocaleTimeString("fr-FR", options).slice(0, -15)} avec {users.filter(user => user.id === reservation.instructorUserId).map(user => user.firstName + ' ' + user.lastName.toUpperCase())}</h2>
                <h3 className={styles.subtitle}>Rendez-vous : </h3>
                <div className={styles.parcour}>
                    <p className={styles.nameChemin}>{places.filter(place => place.id === reservation.placeId).map(place => place.name)} <span className={styles.heure}>{new Date(reservation.dateStart).toLocaleTimeString().slice(0, 2)}h</span></p>
                    <img className={styles.arrowDown} src={`${process.env.PUBLIC_URL}/arrow.svg`} alt="yes"/>
                    <p className={styles.nameChemin}>{places.filter(place => place.id === reservation.placeId).map(place => place.name)} <span className={styles.heure}>{new Date(reservation.dateEnd).toLocaleTimeString().slice(0, 2)}h</span></p>
                </div>
                <div className={styles.first}><span onClick={handleclick} className={styles.check} style={(isFirst)?{background:'#8269DF'}:{background:'#FFF'} }></span> Première heure d'évaluation</div>
                <p className={styles.prix}><span className={styles.subtitle}>Tarif :</span> 40€</p>
                <div className={styles.buttons}>
                  <Popup
                    className={styles.popup}
                    toggler={(
                      <button className={styles.button}>Réserver ce cours</button>
                    )}
                    position="modal"
                  >
                    <h3 className={styles.title}>Confirmation de la réservation</h3>
                    <div className={styles.parcour}>
                        <p className={styles.nameChemin}>{places.filter(place => place.id === reservation.placeId).map(place => place.name)} <span className={styles.heure}>{new Date(reservation.dateStart).toLocaleTimeString().slice(0, 2)}h</span></p>
                        <img className={styles.arrowDown} src={`${process.env.PUBLIC_URL}/arrow.svg`} alt="yes"/>
                        <p className={styles.nameChemin}>{places.filter(place => place.id === reservation.placeId).map(place => place.name)} <span className={styles.heure}>{new Date(reservation.dateEnd).toLocaleTimeString().slice(0, 2)}h</span></p>
                    </div>
                    <p className={styles.prix}><span className={styles.subtitle}>Tarif :</span> 40€</p>
                    <button className={styles.button} data-close>Payer et réserver</button>
                  </Popup>
                  <Link to={'/monitor/'+reservation.instructorUserId} className={styles.button}>Voir le moniteur</Link>
                </div>
                </div>
                <Map className={styles.map} key={reservation.id} idPlaces={[reservation.placeId]} heightPlace={isResponsive ? '30vh' : '100%'} widthPlace={isResponsive ? '100%' : '40vw'}></Map>
            </div>


        }</>
    );
};

export default Single;
