import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Map from "../components/Map";
import Session from "../components/Session/Session";
import arrow from '../assets/icons/arrowOrange.svg';
import styles from '../styles/pages/CoursList.module.scss';

const CoursList = () => {
  const [cours, setCours] = useState([]);
  const [idPlaces, setIdPlaces] = useState();
  const [responsive, setResponsive] = useState(false);

  useEffect(() => {
    setResponsive(window.screen.width <= 480);
    fetch('/api/sessions')
      .then((res) => res.json())
      .then((data) => {
        setCours(data.slice(0, 10));
        setIdPlaces(data.map((session) => session.placeId));
      });
  }, []);

  return (
    <main className={styles.main}>
      <Link to="/student"><img className={styles.arrow} src={arrow} alt="" /></Link>
      <h2 className={styles.title}>Liste des cours disponibles</h2>
      <div className={styles.wrapper}>
        <div className={styles.mapContainer}>
          {idPlaces && (
            <Map
              idPlaces={idPlaces}
              heightPlace={responsive ? '40vh' : '60vh'}
              widthPlace="100%"
            />
          )}
        </div>
        <ul className={styles.list}>
          {cours.map((session) => ( 
            <li key={session.id}>
              <Session session={session} />
            </li>
          ))}
        </ul>
      </div>
    </main>
  );
};

export default CoursList;
